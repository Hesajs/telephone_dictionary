//This code is written in codeblocks with GNU GCC compiler.

#include<stdio.h>
#include<stdlib.h>

struct record
{
    int id;
    char fname[20];
    char lname[20];
    char address[25];
    long phoneNumber;
} user;

FILE *fpt;

void append(void);
void list(void);
void dlt(void);

int main(void)
{
    int option;

    printf("\nChoose one of the option!\n\n");
    printf("1. Append\n2. List\n3. Edit\n4. Delete\n5. Exit\n\n? ");

    //User input of option.
    scanf("%d", &option);

    switch(option){
        case 1:
            append();
            break;
        case 2:
            list();
            break;
        case 3:
            //edit();
            break;
        case 4:
            dlt();
            break;
        case 5:
            break;
        default:
            printf("\nInvalid Option!");
            break;
    }

    return 0;
}

void append(void){
    char flag;
    fpt = fopen("/home/youxin/Project/telephone_dictionary/records.txt", "a");

    //Input from the user to save records.
    do{
        system("clear");
        while((getchar())!='\n');
        printf("\nInput the ID number: ");
        scanf("%d", &user.id);
        printf("\nInput first name: ");
        scanf("%s", user.fname);
        printf("\nInput second name: ");
        scanf("%s", user.lname);
        printf("\nInput Address: ");
        scanf("%s", user.address);
        printf("\nInput Phone Number: ");
        //while((getchar())!='\n');
        scanf("%ld", &user.phoneNumber);

        //to clear the buffer
        while((getchar())!='\n');

        //writing in the file.
        fprintf(fpt, "%d %s %s %s %d\n", user.id, user.fname, user.lname, user.address, user.phoneNumber);

        // %d\t%s\t%s\t%s\t%d\n

        //Terminate the loop as per user requirement.
        printf("\n\nDo you want save more? (Y/n): ");

        flag = getchar();
    }while(flag!='n');
    fclose(fpt);
}

void list(void)
{
    system("clear");
    fpt = fopen("/home/youxin/Project/telephone_dictionary/records.txt", "r");
    printf("\nID\tFirst-Name\tLast-Name\tAddress\t\tPhone Number\n");
    do{
        //fread(&user, sizeof(user),1, fpt);
        fscanf(fpt, "%d %s %s %s %d", &user.id, user.fname, user.lname, user.address, &user.phoneNumber);
        printf("%d %13s %15s %16s %15d\n", user.id, user.fname, user.lname, user.address, user.phoneNumber);
    }while(!feof(fpt));
    fclose(fpt);
}

void dlt(void)
{
    int input, rent;
    FILE *fpt1;
    fpt = fopen("/home/youxin/Project/telephone_dictionary/records.txt", "r");
    fpt1 = fopen("/home/youxin/Project/telephone_dictionary/temp.txt", "w");
    //Listing all the records for the user to select the id to delete.
    list();

    printf("\nEnter the ID number to delete: ");
    scanf("%d", &input);

    do{
        //reading the records
        fscanf(fpt, "%d %s %s %s %d", &user.id, user.fname, user.lname, user.address, &user.phoneNumber);
        printf("user ID=> %d\ninput => %d\n", user.id, input);
        if(input!=user.id){
            //writing in the file.
            fprintf(fpt1, "%d %s %s %s %d\n", user.id, user.fname, user.lname, user.address, user.phoneNumber);
        }
    }while(feof(fpt));

    rent = remove("records.txt");

    if(rent==0){
        rename("temp.txt", "records.txt");
        printf("\n--------------Deleted Successfully!----------------\n\n");
    }else{
        printf("\nUnable to delete the record witd ID no. %d", input);
    }

    //list();

    fclose(fpt1);
    fclose(fpt);
}
